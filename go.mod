module code.videolan.org/videolan/CrashDragon

go 1.14

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/jinzhu/copier v0.2.8
	github.com/jinzhu/gorm v1.9.16
	github.com/lib/pq v1.10.0
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/microcosm-cc/bluemonday v1.0.5
	github.com/russross/blackfriday v1.6.0
	github.com/satori/go.uuid v1.2.0
)
